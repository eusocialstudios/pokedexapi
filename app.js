const express = require('express');
const app = express();
const path = require('path');
const pokedex = require("./pokedex.json");
const HTTP_BAD_REQUEST = 400;
const HTTP_NOT_FOUND = 404;

// --------- Safety Net for API --------- 
function isValid(ndex){
    var toTest = parseInt(ndex);
    if (!isNaN(toTest)){
        if ((toTest >= 1) & (toTest <= 809)) // NDEX is between 1 and 809
        return true;
    }
    return false;
}

// --------- Functions to Get various data ---------  

function getName(ndex){
    if(isValid(ndex)){
        return(pokedex[ndex-1]['name']);
    }
    return HTTP_BAD_REQUEST;
}

function getType(ndex){
    if(isValid(ndex)){
        return(pokedex[ndex-1]['type']);
    }
    return HTTP_BAD_REQUEST;
}

function getBaseStats(ndex){
    if(isValid(ndex)){
        return(pokedex[ndex-1]['base']);
    }
    return HTTP_BAD_REQUEST;
}

function getImagePath(ndex){
    if(isValid(ndex)){
        // Pad the number with leading zeros if needed
        var id = ndex+"";
        while (id.length < 3) id = "0" + id;
        // Construct the filepath
        var img = 'images/' + id + getName(ndex)['english']+'.png';
        return(path.join(__dirname, img));
    }
    return HTTP_BAD_REQUEST;
}

// ----------- API Defintion -----------

app.get('/pokedex/:ndex/name', (req, res) => {
    res.send(getName(req.params.ndex));
});

app.get('/pokedex/:ndex/type', (req, res) => {
    res.send(getType(req.params.ndex));
});

app.get('/pokedex/:ndex/basestats', (req, res) => {
    res.send(getBaseStats(req.params.ndex));
});

app.get('/pokedex/:ndex/image', (req, res) => {
    res.contentType('image/png');
    res.sendFile(getImagePath(req.params.ndex));
});

// ------- API Default Responses --------

app.get('/', (req, res) => {
    res.redirect('https://documenter.getpostman.com/view/6822932/S11Lscpd');
});

app.get('/pokedex', (req, res) => {
    res.redirect('https://documenter.getpostman.com/view/6822932/S11Lscpd');
});

app.use(function(req, res){
    res.sendStatus(HTTP_NOT_FOUND);
});


// ---------- Start the API -----------

app.listen(3000, '0.0.0.0', () => {
    console.log('PokedexAPI running...');
})